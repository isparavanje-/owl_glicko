'''Definition of database models.'''
from peewee import * # pylint: disable=W0401,W0622,W0614
from playhouse.sqlite_ext import CSqliteExtDatabase

import config

if config.test == False:
    DB = CSqliteExtDatabase('overwatch.db')
elif config.test == True:
    DB_disk = CSqliteExtDatabase('overwatch.db')
    DB = CSqliteExtDatabase(':memory:')
    DB_disk.backup(DB)



class BaseModel(Model):
    class Meta:
        database = DB

class Constant(BaseModel):
    '''Ranking system constants model'''
    name = CharField(unique=True)
    constant = FloatField()

class Team(BaseModel):
    '''Team model'''
    name = CharField(unique=True)
    color1 = CharField()
    color2 = CharField()
    color3 = CharField()

class Rating(BaseModel):
    '''Rating model'''
    week = IntegerField()
    team = ForeignKeyField(Team, related_name='ratings')
    rating = FloatField()
    RD = FloatField()

class Match(BaseModel):
    '''Match model'''
    week = IntegerField()
    team1 = ForeignKeyField(Team, related_name='team1')
    team2 = ForeignKeyField(Team, related_name='team2')
    wins = IntegerField()
    draws = IntegerField()
    losses = IntegerField()
    day = IntegerField()

class FutureMatch(BaseModel):
    week = IntegerField()
    team1 = ForeignKeyField(Team, related_name='team1')
    team2 = ForeignKeyField(Team, related_name='team2')
    day = IntegerField()

def add_team(name):
    '''add_team(name)'''
    team = Team.create(name=name)
    return team.save()

def add_match(team1, team2, score, day):
    '''add_match(team1, team2, score)'''
    match = Match.create(team1=team1, team2=team2, wins=score[0], draws=score[1], losses=score[2], day=day)
    return match.save()

def add_rating(week, team, rating, RD):
    rating = Rating.create(week=week, team=team, rating=rating, RD=RD)
    return rating.save()

def add_future_match(week, day, team1, team2):
    future_match = FutureMatch.create(week=week, day=day, team1=team1, team2=team2)
    return future_match.save()

def create_tables():
    DB.create_tables([Constant, Team, Rating, Match, FutureMatch]) 
