'''Script to update ratings'''

import numpy as np
import db_model as dm
from numba import jit


def initialize():
    MAXRD = dm.Constant.get(dm.Constant.name == 'MAXRD').constant
    query = dm.Rating.delete()
    query.execute() # pylint: disable=E1120
    teams = dm.Team.select()
    for team in teams:
        dm.add_rating(-1, team, 1500, MAXRD)

@jit
def g(RD):
    Q = np.log(10)/400
    return 1/np.sqrt(1+3*Q**2*RD**2/np.pi**2)

@jit
def E(RD, rating1, rating2):
    return 1/(1+10**(-g(RD)*(rating1-rating2)/400))

def get_ratings(team, week):
    C = dm.Constant.get(dm.Constant.name == 'C').constant
    rating = dm.Rating.get(team=team, week=week)
    return (rating.rating, np.sqrt(rating.RD**2 + C**2))

def glicko_ratings(week, last_day, team_rating, team, opponent_ratings, output_Z, constants):
    #(C, K, MAXRD, B1, B2) = constants
    Q = np.log(10)/400
    matches_1 = dm.Match.select().where(dm.Match.week == week, dm.Match.day <= last_day, dm.Match.team1 == team)
    matches_2 = dm.Match.select().where(dm.Match.week == week, dm.Match.day <= last_day, dm.Match.team2 == team)
    r_sum = 0
    d_sum = 0
    matches = 0
    for match in matches_1:
        matches = matches + 1
        wins = match.wins
        draws = match.draws
        losses = match.losses
        opponent_rating = opponent_ratings[match.team2.name][0]
        opponent_RD = opponent_ratings[match.team2.name][1]
        E_value = E(opponent_RD, team_rating[0], opponent_rating)
        g_value = g(opponent_RD)
        r_sum = r_sum + g_value*((1-E_value)*wins+(0.5-E_value)*draws+(0-E_value)*losses)
        d_sum = d_sum + g_value**2*E_value*(1-E_value)
    for match in matches_2:
        matches = matches + 1
        wins = match.losses
        draws = match.draws
        losses = match.wins
        opponent_rating = opponent_ratings[match.team1.name][0]
        opponent_RD = opponent_ratings[match.team1.name][1]
        E_value = E(opponent_RD, team_rating[0], opponent_rating)
        g_value = g(opponent_RD)
        r_sum = r_sum + g_value*((1-E_value)*wins+(0.5-E_value)*draws+(0-E_value)*losses)
        d_sum = d_sum + g_value**2*E_value*(1-E_value)
    if output_Z == 0:
        if matches != 0:
            d2 = 1/(Q**2*d_sum)
            rating_new = team_rating[0] + Q/(1/team_rating[1]**2 + 1/d2)*r_sum
            RD_new = np.sqrt(1/(1/team_rating[1]**2 + 1/d2))
        else: 
            rating_new = team_rating[0]
            RD_new = team_rating[1]
        return (rating_new, RD_new)
    elif output_Z == 1:
        if matches !=0:
            return r_sum/np.sqrt(d_sum)
        else:
            return 0

def update(current_week, last_day):
    C = dm.Constant.get(dm.Constant.name == 'C').constant
    K = dm.Constant.get(dm.Constant.name == 'K').constant
    MAXRD = dm.Constant.get(dm.Constant.name == 'MAXRD').constant
    B1 = dm.Constant.get(dm.Constant.name == 'B1').constant
    B2 = dm.Constant.get(dm.Constant.name == 'B2').constant
    constants = (C, K, MAXRD, B1, B2)
    teams = dm.Team.select()
    ratings_old = {}
    for team in teams:
        ratings_old[team.name] = get_ratings(team, current_week-1)
    #glicko-boost step 1
    ratings_1 = {}
    for team in teams:
        ratings_1[team.name] = glicko_ratings(current_week, last_day, ratings_old[team.name], team, ratings_old, 0, constants)
    #glicko-boost step 2
    ratings_2 = {}
    for team in teams:
        ratings_2[team.name] = glicko_ratings(current_week, last_day, ratings_old[team.name], team, ratings_1, 0, constants)
    #glicko-boost step 3
    ratings_boosted = {}
    for team in teams:
        Z = glicko_ratings(current_week, last_day, ratings_2[team.name], team, ratings_2, 1, constants)
        if Z>K:
            #print('{}, {}'.format(ratings_old[team.name][1], (1+(Z-K)*B1)*ratings_old[team.name][1]+B2))
            ratings_boosted[team.name] = (ratings_old[team.name][0], min((1+(Z-K)*B1)*ratings_old[team.name][1]+B2, MAXRD))
        else:
            ratings_boosted[team.name] = (ratings_old[team.name][0], min(ratings_old[team.name][1], MAXRD))
    #glicko-boost step 4
    ratings_3 = {}
    for team in teams:
        ratings_3[team.name] = glicko_ratings(current_week, last_day, ratings_boosted[team.name], team, ratings_boosted, 0, constants)
    #glicko-boost step 5
    ratings_final = {}
    for team in teams:
        ratings_final[team.name] = glicko_ratings(current_week, last_day, ratings_boosted[team.name], team, ratings_3, 0, constants)
    query = dm.Rating.delete().where(dm.Rating.week == current_week)
    query.execute()
    for team in ratings_final:
        dm.add_rating(current_week, dm.Team.get(dm.Team.name == team), *ratings_final[team])
