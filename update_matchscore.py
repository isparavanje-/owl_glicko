'''Database match entry updating script'''

import csv
import db_model as dm
import config

matchlist = [] # pylint: disable=C0103

if config.test == False:
    filename = 'matches.csv'
elif config.test == True:
    filename = 'matches_all.csv'

with open(filename) as csvfile:
    reader = csv.reader(csvfile, delimiter=',') # pylint: disable=C0103
    for row in reader:
        matchlist.append(row)

match_update_list = [] # pylint: disable=C0103

for row in matchlist:
    team1 = dm.Team.get(dm.Team.name == row[1])
    team2 = dm.Team.get(dm.Team.name == row[2])
    match_update_list.append(dm.Match.create(week=row[0], team1=team1,
                                             team2=team2, wins=row[3],
                                             draws=row[4], losses=row[5], day=row[6]))

for match in match_update_list:
    match.save()
