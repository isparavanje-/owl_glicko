'''Database team entry updating script'''

import csv
import db_model as dm

teamlist = [] # pylint: disable=C0103

with open('teams.csv') as csvfile:
    reader = csv.reader(csvfile, delimiter=',') # pylint: disable=C0103
    for row in reader:
        teamlist.append(row)

team_update_list = [] # pylint: disable=C0103

for row in teamlist:
    team_update_list.append(dm.Team.create(name=row[0], color1=row[1], color2=row[2], color3=row[3]))

for team in team_update_list:
    team.save()
