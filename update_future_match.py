'''Database match entry updating script'''

import csv
import db_model as dm

matchlist = [] # pylint: disable=C0103

with open('futurematches.csv') as csvfile:
    reader = csv.reader(csvfile, delimiter=',') # pylint: disable=C0103
    for row in reader:
        matchlist.append(row)

match_update_list = [] # pylint: disable=C0103

for row in matchlist:
    team1 = dm.Team.get(dm.Team.name == row[1])
    team2 = dm.Team.get(dm.Team.name == row[2])
    match_update_list.append(dm.FutureMatch.create(week=row[0], team1=team1,
                                             team2=team2, day=row[3]))

for match in match_update_list:
    match.save()
